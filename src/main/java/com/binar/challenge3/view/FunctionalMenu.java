package com.binar.challenge3.view;

@FunctionalInterface
public interface FunctionalMenu {
    void runMenu();
}
