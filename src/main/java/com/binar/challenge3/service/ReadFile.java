package com.binar.challenge3.service;

import java.io.IOException;
import java.util.List;

public abstract class ReadFile {
    public static String PATH_FILE_READ = "src/main/resources/data_sekolah.csv";

    public abstract List<Integer> readFile() throws IOException;

}
