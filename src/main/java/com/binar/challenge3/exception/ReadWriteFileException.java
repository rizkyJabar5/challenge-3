package com.binar.challenge3.exception;

import com.binar.challenge3.service.ReadFile;

import java.io.FileNotFoundException;

public class ReadWriteFileException extends FileNotFoundException {
    public ReadWriteFileException() {
        super("File tidak ada nih bestie, silahkan dicek di " + ReadFile.PATH_FILE_READ);
    }

    public ReadWriteFileException(String message) {
        super(message);
    }
}
