package com.binar.challenge3.data;

import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.stream.DoubleStream;

@Getter
public class Calculating {

    public Map<Integer, Integer> frequency(List<Integer> freqList) {

        Map<Integer, Integer> frequency = new HashMap<>();

        for (Integer s : freqList) {
            Integer count = frequency.get(s);
            if(count == null){
                count = 0;
            }
            frequency.put(s, count + 1);
        }
        return frequency;
    }

    // Fungsi untuk mencari modus dari nilai yang ada di list
    public int modus(List<Integer> listGrades) {

        HashMap<Integer, Integer> hMap = new HashMap<>();
        int max = 1;
        int modus = 0;
        for(Integer num : listGrades){
            if(hMap.get(num) != null){
                int count = hMap.get(num);
                count ++;
                hMap.put(num, count);
                if(count > max){
                    max = count;
                    modus = num;
                }
            } else hMap.put(num, 1);
        }
        return modus;
    }

// ----------------Fungsi untuk mencari median dengan Stream-------------------
    public double median(List<Integer> listGrades) {
        DoubleStream sortedNumbers = listGrades
                .stream()
                .mapToDouble(v -> v)
                .sorted();
        OptionalDouble median = (
               listGrades.size() % 2 == 0 ?
                sortedNumbers.skip((listGrades.size() / 2 ) - 1)
                        .limit(2)
                        .average() : sortedNumbers.skip(listGrades.size() / 2)
                        .findFirst()
                );
        return median.orElse(Double.NaN);
    }

//    ----------------------Fungsi untuk menghitung Mean -> dengan Stream-------------
    public double mean(List<Integer> listGrades) {
        return listGrades.stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0.0);
    }
}
