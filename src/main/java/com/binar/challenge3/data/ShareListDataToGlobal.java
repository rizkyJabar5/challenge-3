package com.binar.challenge3.data;

import com.binar.challenge3.service.ReadFileImpl;
import lombok.Getter;

import java.util.List;

@Getter
public class ShareListDataToGlobal {
    private final ReadFileImpl rf = new ReadFileImpl();
    public List<Integer> listGrades = rf.readFile();
}
