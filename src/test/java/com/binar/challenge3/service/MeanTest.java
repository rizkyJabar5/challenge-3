                            package com.binar.challenge3.service;

                            import com.binar.challenge3.data.Calculating;
                            import com.binar.challenge3.data.ShareListDataToGlobal;
                            import org.junit.Test;
                            import org.junit.jupiter.api.Assertions;
                            import org.junit.jupiter.api.DisplayName;

                            import java.util.List;

public class MeanTest {

    Calculating calc = new Calculating();
    ShareListDataToGlobal data = new ShareListDataToGlobal();
    List<Integer> listOfNull = null;

    @Test
    @DisplayName("Mean is right a value")
    public void meanRightResult(){

        double expectedValue = 8.318181818181818;
        Assertions.assertEquals(expectedValue, calc.mean(data.getListGrades()));
    }

    @Test
    @DisplayName("Mean is list of null")
    public void meanIsWrongResult(){
        Exception e = Assertions.assertThrows(NullPointerException.class, ()-> calc.mean(listOfNull));
        e.getMessage();
    }
}
