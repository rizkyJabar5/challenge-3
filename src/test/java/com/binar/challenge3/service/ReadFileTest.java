package com.binar.challenge3.service;

import com.binar.challenge3.exception.ReadWriteFileException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;

import static com.binar.challenge3.service.ReadFile.PATH_FILE_READ;

public class ReadFileTest {

    String pathFileUnknown = "source/path/lhokokgakada.txt";


    @Test
    @DisplayName("Pengecekan File ketika ada")
    void readFileExist(){
        File tempFile = new File(PATH_FILE_READ);
        boolean existsOrNot = tempFile.exists();
        System.out.println("Status file: " + existsOrNot);
        if(!tempFile.exists()){
            Assertions.assertThrows(ReadWriteFileException.class,
                    () -> Assertions.assertTrue(true));
        }
    }

    @Test
    @DisplayName("Pengecekan File ketika tidak ada")
    void readFileNotExist(){
        File tempFile = new File(pathFileUnknown);
        boolean existsOrNot = tempFile.exists();
        System.out.println("Status file: " + existsOrNot);
        Assertions.assertFalse(existsOrNot);


    }
}